var mongoose = require("mongoose");
var validator = require("validator")
var User = mongoose.model('user',{
    email : {
        type : String,
        trim :true,
        minlength : 1,
        required : true,
        unique : true,
        validate : {
            validator :validator.isEmail,
            message :"{value} is valid email"
        }
    },
    password :{
        type:String,
        minlength:6,
        required :true
    },
    tokens: [{
        access: {
            type :String,
            required:true
        },
        token : {
            type : String,
            required :true,
        }
    }]
});
module.exports.User = User;