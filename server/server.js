

var express = require('express');
var bodyParse = require('body-parser');
var _ = require("lodash");

var {mongoose} = require('./db/mongoose');
var {Todo} = require('../model/todo');
var {User} = require('../model/user');

var app =express();
app.use(bodyParse.json());
app.post("/todos",(req,res) =>{
//console.log(req.body);
var todo = new Todo({
    text:req.body.text
}) 
    todo.save().then((result) =>{
        res.send(result);
    },(err) =>{
        res.send(err);
    })
});

app.get('/todo/:id',(req,res) =>{
    var id = req.params.id;
    Todo.findById(id).then((result) => {
        if(!result){
            console.log("id not found")
        }else{
         res.send(JSON.stringify( result) );
        }
     },(err) =>{
         console.log("err");
     });
});

app.post('/users' ,(req,res) =>{
    var body = _.pick(req.body,['email','password'])
    var user = new User(body);
    user.save().then((user) =>{
        res.send(user);
    }).catch((e)=>{
        res.status(400).send(e);
    })
})
app.listen(3000, () =>{
    console.log("server is starting...........")
})
