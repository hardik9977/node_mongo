var {MongoClient ,ObjectID} = require("mongodb");

MongoClient.connect('mongodb://localhost:27017/TodoApp',(err,db) =>{
if(err){
    console.log("unable to connect to mongodb server")
}else{
    //delete many
    // console.log("Connected to sever");
    // db.collection('Todos').deleteMany({ text : "Eat lunch"}).then((result) =>{
    //     console.log(result);
    // },(err) => {
    //     console.log(err);
    // })

    //deleteOne findoneandDelete
    // db.collection('Todos').deleteOne({ text : "Eat lunch"}).then((result) =>{
    //     console.log(result);
    // },(err) => {
    //     console.log(err);
    // })

    console.log("Connected to sever");
    db.collection('Todos').findOneAndDelete({ completed : false}).then((result) =>{
        console.log(result);
    },(err) => {
        console.log(err);
    })
    db.close();
}
});